// Open Main Header
function openMainHeader() {
  //Tạo biến current value để chứa giá trị bật tắt hiện tại
  currentValue = document.querySelector(".vertical__header .button__responsive").value;
  //Nếu trí trị hiện tại bằng OFF thì giá trị của thẻ div sẽ thay đổi thành ON và ngược lại
  if (currentValue == "Off") {
    document.querySelector(".vertical__header .button__responsive").value = "On";
    document.querySelector('body').style.overflow = "";
    $(".vertical__header").removeClass("on__header");
    $(".main__header").removeClass("on__header");
    $(".page__wrap").removeClass("on__header");
  } else {
    document.querySelector(".vertical__header .button__responsive").value = "Off";
    document.querySelector('body').style.overflow = "hidden";
    $(".vertical__header").addClass("on__header");
    $(".main__header").addClass("on__header");
    $(".page__wrap").addClass("on__header")
  }
}

// Shrink Header
$(function () {
  var shrinkNothing = 300;
  var shrinkHeader = 500;
  $(window).scroll(function () {
    var scroll = getCurrentScroll();
    if (scroll >= shrinkNothing && scroll < shrinkHeader) {
      $("header").addClass("nothing");
    } else if (scroll >= shrinkHeader) {
      $("header").removeClass("nothing");
      $("header").addClass("shrink shadow-sm");
    } else {
      $("header").removeClass("nothing");
      $("header").removeClass("shrink shadow-sm");
    }
  });
  function getCurrentScroll() {
    return window.pageYOffset;
  }
});

// Form options
function openFormOptions() {
  //Tạo biến current value để chứa giá trị bật tắt hiện tại
  currentValue = document.querySelector(".form__courses .options").value;
  //Nếu trí trị hiện tại bằng OFF thì giá trị của thẻ div sẽ thay đổi thành ON và ngược lại
  if (currentValue == "Off") {
    document.querySelector(".form__courses .options").value = "On";
    $(".form__courses .options").removeClass("d-none");
    $(".form__courses").addClass("angle");
  } else {
    document.querySelector(".form__courses .options").value = "Off";
    $(".form__courses .options").addClass("d-none");
    $(".form__courses").removeClass("angle");
  }
}
